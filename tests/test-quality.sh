#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

# shellcheck disable=1090
source "${PROJECT_DIR}/lib/ansi/ansi"

if [[ ! -f "${PROJECT_DIR}/lib/shellcheck/shellcheck" ]]; then
  ansi --bold --red " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  ansi --bold --red " ┃ Quality check requires ShellCheck (https://github.com/koalaman/shellcheck)   "
  ansi --bold --red " ┃ to be installed! Use 'make setup' to install required dependencies.          "
  ansi --bold --red " ┃ Aborting...                                                                  "
  ansi --bold --red " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  exit 1
fi

if find "${PROJECT_DIR}" -name "*.sh" -print0 | xargs -0 "${PROJECT_DIR}/lib/shellcheck/shellcheck"; then
  ansi --bold --green " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  ansi --bold --green " ┃ QUALITY -- Perfect!                                                          "
  ansi --bold --green " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
else
  ansi --bold --red   " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  ansi --bold --red   " ┃ QUALITY -- There is still some work to do...                                 "
  ansi --bold --red   " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  exit 1
fi

exit 0
