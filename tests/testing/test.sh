#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/ndd-utils4b.sh"



echo "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ tests/testing/test.sh ━━━━━"



setUp() {
  :
}



# ------------------------------------------------------------------------------
test_assertStringIsEmpty() {

  assertStringIsEmpty ""

  # Don't know how to test that...
  # if (assertStringIsEmpty "not empty"); then
  #   fail "assertStringIsEmpty must fail when not empty"
  # fi
}

# ------------------------------------------------------------------------------
test_assertStringIsNotEmpty() {

  assertStringIsNotEmpty "actually not empty"

  # Don't know how to test that...
  # if (assertStringIsNotEmpty ""); then
  #   fail "assertStringIsNotEmpty must fail when empty"
  # fi
}

# ------------------------------------------------------------------------------
test_assertFileExists() {

  assertFileExists "${BASH_SOURCE[0]}"

  if (assertFileExists "${BASH_SOURCE[0]}/does-no-exist"); then
    fail "assertFileExists must fail when file does not exist"
  fi
  if (assertFileExists "${PROJECT_DIR}"); then
    fail "assertFileExists must fail when not a file"
  fi
}

# ------------------------------------------------------------------------------
test_assertFileDoesNotExist() {

  assertFileDoesNotExist "${BASH_SOURCE[0]}/does-no-exist"
  assertFileDoesNotExist "${PROJECT_DIR}"

  if (assertFileDoesNotExist "${BASH_SOURCE[0]}"); then
    fail "assertFileExists must fail when file does exist"
  fi
}

# ------------------------------------------------------------------------------
test_assertDirectoryExists() {

  assertDirectoryExists "${PROJECT_DIR}"

  if (assertDirectoryExists "${BASH_SOURCE[0]}/does-no-exist"); then
    fail "assertDirectoryExists must fail when directory does not exist"
  fi
  if (assertDirectoryExists "${BASH_SOURCE[0]}"); then
    fail "assertDirectoryExists must fail when not a directory"
  fi
}

# ------------------------------------------------------------------------------
test_assertDirectoryDoesNotExist() {

  assertDirectoryDoesNotExist "${BASH_SOURCE[0]}/does-no-exist"
  assertDirectoryDoesNotExist "${BASH_SOURCE[0]}"

  if (assertDirectoryDoesNotExist "${PROJECT_DIR}"); then
    fail "assertDirectoryDoesNotExist must fail when directory does exist"
  fi
}

# ------------------------------------------------------------------------------
test_visual_check() {

  # echo "assertStringIsEmpty will succeed:"
  # assertStringIsEmpty ""
  # echo "assertStringIsEmpty will fail:"
  # assertStringIsEmpty 'actually not empty'

  # assertFileExists "${BASH_SOURCE[0]}"
  # assertFileExists "${BASH_SOURCE[0]}/does-no-exist"
  # assertFileExists "${PROJECT_DIR}"

  # assertFileDoesNotExist "${BASH_SOURCE[0]}/does-no-exist"
  # assertFileDoesNotExist "${BASH_SOURCE[0]}"

  # assertDirectoryExists "${PROJECT_DIR}"
  # assertDirectoryExists "${BASH_SOURCE[0]}/does-no-exist"
  # assertDirectoryExists "${BASH_SOURCE[0]}"

  # assertDirectoryDoesNotExist "${BASH_SOURCE[0]}/does-no-exist"
  # assertDirectoryDoesNotExist "${PROJECT_DIR}"

  :
}



# ------------------------------------------------------------------------------

# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/shunit2/shunit2"
