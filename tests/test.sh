#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

# shellcheck disable=1090
source "${PROJECT_DIR}/lib/ansi/ansi"

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    ansi --bold --red " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    ansi --bold --red " ┃ TESTS -- Failed!                                                             "
    ansi --bold --red " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"

    exit 1
}

trap 'error_handler ${?}' ERR

"${PROJECT_DIR}/tests/base/test.sh"
"${PROJECT_DIR}/tests/other/test.sh"
"${PROJECT_DIR}/tests/print/test.sh"
"${PROJECT_DIR}/tests/strings/test.sh"
"${PROJECT_DIR}/tests/symbols/test.sh"
"${PROJECT_DIR}/tests/testing/test.sh"
"${PROJECT_DIR}/tests/versions/test.sh"

ansi --bold --green " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
ansi --bold --green " ┃ TESTS -- Perfect!                                                            "
ansi --bold --green " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"

exit 0
