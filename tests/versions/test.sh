#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/ndd-utils4b.sh"



echo "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ tests/versions/test.sh ━━━━━"



setUp() {
  :
}



# ------------------------------------------------------------------------------
test_is_semantic_version() {

  local valid_versions=(
    "1.0.0"
    "1.2.3"
    "11.22.33"
  )

  local invalid_versions=(
    "a.b.c"
    "a.2.3"
    "1.b.3"
    "1.2.c"
    "1.2.3.4"
    "1a.2.3"
    "1.2b.3"
    "1.2.3c"
  )

  for valid_version in "${valid_versions[@]}"; do
    ndd::versions::is_semantic_version "${valid_version}" || fail "ndd::versions::is_semantic_version '${valid_version}'"
  done

  for invalid_version in "${invalid_versions[@]}"; do
    ! ndd::versions::is_semantic_version "${invalid_version}" || fail "ndd::versions::is_semantic_version '${invalid_version}'"
  done
}

# ------------------------------------------------------------------------------
test_is_greater() {

  local valid_versions_pairs=(
    "1.0.1 1.0.0"
    "1.1.0 1.0.0"
    "2.0.0 1.0.0"
  )

  local invalid_versions_pairs=(
    "1.0.0 1.0.0"
    "1.0.0 1.0.1"
    "1.0.0 1.1.0"
    "1.0.0 2.0.0"

    "1.0.0 a.0.0"
    "a.0.0 2.0.0"
    "a.0.0 a.0.0"
  )

  for valid_versions_pair in "${valid_versions_pairs[@]}"; do
    # shellcheck disable=SC2206
    local valid_versions=(${valid_versions_pair})
    ndd::versions::is_greater "${valid_versions[0]}" "${valid_versions[1]}" || fail "ndd::versions::is_greater '${valid_versions[0]}' '${valid_versions[1]}'"
  done

  for invalid_versions_pair in "${invalid_versions_pairs[@]}"; do
    # shellcheck disable=SC2206
    local invalid_versions=(${invalid_versions_pair})
    ! ndd::versions::is_greater "${invalid_versions[0]}" "${invalid_versions[1]}" || fail "ndd::versions::is_greater '${invalid_versions[0]}' '${invalid_versions[1]}'"
  done
}

# ------------------------------------------------------------------------------
test_is_greater_or_equal() {

  local valid_versions_pairs=(
    "1.0.0 1.0.0"
    "1.0.1 1.0.0"
    "1.1.0 1.0.0"
    "2.0.0 1.0.0"
  )

  local invalid_versions_pairs=(
    "1.0.0 1.0.1"
    "1.0.0 1.1.0"
    "1.0.0 2.0.0"

    "1.0.0 a.0.0"
    "a.0.0 2.0.0"
    "a.0.0 a.0.0"
  )

  for valid_versions_pair in "${valid_versions_pairs[@]}"; do
    # shellcheck disable=SC2206
    local valid_versions=(${valid_versions_pair})
    ndd::versions::is_greater_or_equal "${valid_versions[0]}" "${valid_versions[1]}" || fail "ndd::versions::is_greater_or_equal '${valid_versions[0]}' '${valid_versions[1]}'"
  done

  for invalid_versions_pair in "${invalid_versions_pairs[@]}"; do
    # shellcheck disable=SC2206
    local invalid_versions=(${invalid_versions_pair})
    ! ndd::versions::is_greater_or_equal "${invalid_versions[0]}" "${invalid_versions[1]}" || fail "ndd::versions::is_greater_or_equal '${invalid_versions[0]}' '${invalid_versions[1]}'"
  done
}

# ------------------------------------------------------------------------------
test_is_less() {

  local valid_versions_pairs=(
    "1.0.0 1.0.1"
    "1.0.0 1.1.0"
    "1.0.0 2.0.0"
  )

  local invalid_versions_pairs=(
    "1.0.0 1.0.0"
    "1.0.1 1.0.0"
    "1.1.0 1.0.0"
    "2.0.0 1.0.0"

    "1.0.0 a.0.0"
    "a.0.0 2.0.0"
    "a.0.0 a.0.0"
  )

  for valid_versions_pair in "${valid_versions_pairs[@]}"; do
    # shellcheck disable=SC2206
    local valid_versions=(${valid_versions_pair})
    ndd::versions::is_less "${valid_versions[0]}" "${valid_versions[1]}" || fail "ndd::versions::test_is_greater_or_equal '${valid_versions[0]}' '${valid_versions[1]}'"
  done

  for invalid_versions_pair in "${invalid_versions_pairs[@]}"; do
    # shellcheck disable=SC2206
    local invalid_versions=(${invalid_versions_pair})
    ! ndd::versions::is_less "${invalid_versions[0]}" "${invalid_versions[1]}" || fail "ndd::versions::test_is_greater_or_equal '${invalid_versions[0]}' '${invalid_versions[1]}'"
  done
}

# ------------------------------------------------------------------------------
test_is_less_or_equal() {

  local valid_versions_pairs=(
    "1.0.0 1.0.0"
    "1.0.0 1.0.1"
    "1.0.0 1.1.0"
    "1.0.0 2.0.0"
  )

  local invalid_versions_pairs=(
    "1.0.1 1.0.0"
    "1.1.0 1.0.0"
    "2.0.0 1.0.0"

    "1.0.0 a.0.0"
    "a.0.0 2.0.0"
    "a.0.0 a.0.0"
  )

  for valid_versions_pair in "${valid_versions_pairs[@]}"; do
    # shellcheck disable=SC2206
    local valid_versions=(${valid_versions_pair})
    ndd::versions::is_less_or_equal "${valid_versions[0]}" "${valid_versions[1]}" || fail "ndd::versions::is_less_or_equal '${valid_versions[0]}' '${valid_versions[1]}'"
  done

  for invalid_versions_pair in "${invalid_versions_pairs[@]}"; do
    # shellcheck disable=SC2206
    local invalid_versions=(${invalid_versions_pair})
    ! ndd::versions::is_less_or_equal "${invalid_versions[0]}" "${invalid_versions[1]}" || fail "ndd::versions::is_less_or_equal '${invalid_versions[0]}' '${invalid_versions[1]}'"
  done
}



# ------------------------------------------------------------------------------

# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/shunit2/shunit2"
