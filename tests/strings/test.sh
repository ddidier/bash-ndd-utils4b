#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/ndd-utils4b.sh"



echo "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ tests/strings/test.sh ━━━━━"



setUp() {
  :
}



# ------------------------------------------------------------------------------
test_to_lower() {
  assertEquals "lower case" "$(ndd::strings::to_lower "Lower case")"
  assertEquals "lower case" "$(ndd::strings::to_lower "LOWER CASE")"
}

# ------------------------------------------------------------------------------
test_to_upper() {
  assertEquals "UPPER CASE" "$(ndd::strings::to_upper "Upper case")"
  assertEquals "UPPER CASE" "$(ndd::strings::to_upper "UPPER CASE")"
}



# ------------------------------------------------------------------------------

# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/shunit2/shunit2"
