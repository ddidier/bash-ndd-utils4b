#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/ndd-utils4b.sh"



echo "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ tests/print/test.sh ━━━━━"



setUp() {
  :
}



# ------------------------------------------------------------------------------
test_script_output_start() {
  assertEquals "▼ ~~~~~ start of output ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ▼" "$(ndd::print::script_output_start)"
}

# ------------------------------------------------------------------------------
test_script_output_end() {
  assertEquals "▲ ~~~~~ end of output ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ▲" "$(ndd::print::script_output_end)"
}



# ------------------------------------------------------------------------------
test_visual_check() {
  ndd::print::script_output_start
  echo "Some random text"
  echo "Some random text Some random text"
  echo "Some random text"
  ndd::print::script_output_end
}



# ------------------------------------------------------------------------------

# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/shunit2/shunit2"
