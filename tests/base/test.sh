#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/ndd-utils4b.sh"



echo "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ tests/base/test.sh ━━━━━"



setUp() {
  :
}



# ------------------------------------------------------------------------------
test_catch_more_errors() {
  # Don't know how to test that...
  ndd::base::catch_more_errors_on
  ndd::base::catch_more_errors_off
}



# ------------------------------------------------------------------------------
test_print_stack_trace() {
  local line_of_call
  local actual

  line_of_call=$(( LINENO + 1))
  actual="$(ndd::base::print_stack_trace 2>&1 > /dev/null)"
  actual="$(echo "${actual}" | sed -E 's@'"${PROJECT_DIR}"'@PROJECT_DIR@g')"
  assertEquals "    in test_print_stack_trace (PROJECT_DIR/tests/base/test.sh:${line_of_call})
    in _shunit_execSuite (PROJECT_DIR/lib/shunit2-v2.1.8/shunit2:1057)
    in source (PROJECT_DIR/lib/shunit2-v2.1.8/shunit2:1331)
    in main (PROJECT_DIR/tests/base/test.sh:${line_of_shunit2_call})" "${actual}"

  line_of_call=$(( LINENO + 1))
  actual="$(ndd::base::print_stack_trace 1 2>&1 > /dev/null)"
  actual="$(echo "${actual}" | sed -E 's@'"${PROJECT_DIR}"'@PROJECT_DIR@g')"
  assertEquals "    in _shunit_execSuite (PROJECT_DIR/lib/shunit2-v2.1.8/shunit2:1057)
    in source (PROJECT_DIR/lib/shunit2-v2.1.8/shunit2:1331)
    in main (PROJECT_DIR/tests/base/test.sh:${line_of_shunit2_call})" "${actual}"
}



# ------------------------------------------------------------------------------
test_visual_check() {
  # ndd::base::print_stack_trace
  :
}



# ------------------------------------------------------------------------------

line_of_shunit2_call=$((LINENO + 2))
# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/shunit2/shunit2"
