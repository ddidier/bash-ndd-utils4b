#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/src/ndd-utils4b.sh"



echo "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ tests/symbols/test.sh ━━━━━"



setUp() {
  :
}



# ------------------------------------------------------------------------------
test_check_mark() {
  assertEquals "✔️" "$(ndd::symbols::check_mark)"
  assertEquals "✔️  message" "$(ndd::symbols::check_mark "message")"
}
# ------------------------------------------------------------------------------
test_cross_mark() {
    assertEquals "❌" "$(ndd::symbols::cross_mark)"
    assertEquals "❌ message" "$(ndd::symbols::cross_mark "message")"
}
# ------------------------------------------------------------------------------
test_exclamation_mark() {
    assertEquals "❗" "$(ndd::symbols::exclamation_mark)"
    assertEquals "❗ message" "$(ndd::symbols::exclamation_mark "message")"
}



# ------------------------------------------------------------------------------
test_success() {
    assertEquals "✅" "$(ndd::symbols::success)"
    assertEquals "✅ message" "$(ndd::symbols::success "message")"
}
# ------------------------------------------------------------------------------
test_failure() {
    assertEquals "❌" "$(ndd::symbols::failure)"
    assertEquals "❌ message" "$(ndd::symbols::failure "message")"
}



# ------------------------------------------------------------------------------
test_debug() {
    assertEquals "🐜" "$(ndd::symbols::debug)"
    assertEquals "🐜 message" "$(ndd::symbols::debug "message")"
}
# ------------------------------------------------------------------------------
test_information() {
    assertEquals "ℹ️" "$(ndd::symbols::information)"
    assertEquals "ℹ️  message" "$(ndd::symbols::information "message")"
}
# ------------------------------------------------------------------------------
test_warning() {
    assertEquals "⚠️" "$(ndd::symbols::warning)"
    assertEquals "⚠️  message" "$(ndd::symbols::warning "message")"
}
# ------------------------------------------------------------------------------
test_error() {
    assertEquals "🔥" "$(ndd::symbols::error)"
    assertEquals "🔥 message" "$(ndd::symbols::error "message")"
}
# ------------------------------------------------------------------------------
test_fatal() {
    assertEquals "💥" "$(ndd::symbols::fatal)"
    assertEquals "💥 message" "$(ndd::symbols::fatal "message")"
}



# ------------------------------------------------------------------------------
test_gear() {
    assertEquals "⚙️ " "$(ndd::symbols::gear)"
}



# ------------------------------------------------------------------------------
test_visual_check() {
    ndd::symbols::check_mark
    ndd::symbols::cross_mark
    ndd::symbols::exclamation_mark

    ndd::symbols::success
    ndd::symbols::failure

    ndd::symbols::debug
    ndd::symbols::information
    ndd::symbols::warning
    ndd::symbols::error
    ndd::symbols::fatal

    ndd::symbols::gear

    # -----

    ndd::symbols::check_mark "message"
    ndd::symbols::cross_mark "message"
    ndd::symbols::exclamation_mark "message"

    ndd::symbols::success "message"
    ndd::symbols::failure "message"

    ndd::symbols::debug "message"
    ndd::symbols::information "message"
    ndd::symbols::warning "message"
    ndd::symbols::error "message"
    ndd::symbols::fatal "message"

    ndd::symbols::gear "message"
}



# ------------------------------------------------------------------------------

# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/shunit2/shunit2"
