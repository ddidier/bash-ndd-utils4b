
# NDD Utils4B

## Version 0.3.0

- fix: rename namespace 'ndd:symbols' to 'ndd::symbols'

## Version 0.2.0

- feat: add `ndd::versions` functions:

    - `ndd::versions::is_greater`
    - `ndd::versions::is_greater_or_equal`
    - `ndd::versions::is_less`
    - `ndd::versions::is_less_or_equal`

- test: add missing tests

## Version 0.1.1

- Fix `ndd::base::print_stack_trace`

## Version 0.1.0

- Initial release
