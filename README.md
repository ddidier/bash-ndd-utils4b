
# NDD Utils4B

A simple utility library for Bash, to keep things dry.

<!-- MarkdownTOC -->

1. [Requirements](#requirements)
1. [Installation](#installation)
1. [Usage](#usage)
1. [Development](#development)
    1. [Installing](#installing)
    1. [Coding](#coding)

<!-- /MarkdownTOC -->



<a id="requirements"></a>
## Requirements

This library has been tested with with Bash 5.x.
You can display your Bash version `bash -c 'echo "${BASH_VERSION}"'`.



<a id="installation"></a>
## Installation

[Download a release](https://gitlab.com/ddidier/bash-ndd-utils4b/-/releases) and extract the files somewhere in your project.



<a id="usage"></a>
## Usage

Source `ndd-utils4b.sh` in your script:

```bash
source ndd-utils4b.sh
```



<a id="development"></a>
## Development

<a id="installing"></a>
### Installing

The development of this library requires the following tools:

- [ShellCheck] for the static analysis of the code
- [Bashcov] for the code coverage (this is a Ruby gem so you should use [RVM])

<a id="coding"></a>
### Coding

The `Makefile` provide some useful targets:

- `make setup` to install the development tools ([RVM] must be installed)
- `make quality` to run the quality checks ([ShellCheck])
- `make test` to run the tests
- `make package` to package the library
- `make` is the default target and is a shortcut to `make quality test`





[ANSI]: https://github.com/fidian/ansi/
[Bashcov]: https://github.com/infertux/bashcov/
[RVM]: https://rvm.io/
[ShellCheck]: https://github.com/koalaman/shellcheck/
