require 'simplecov'
require 'simplecov-cobertura'

SimpleCov.start do
    add_filter "/.git/"
    add_filter "/bin/"
    add_filter "/dist/"
    add_filter "/lib/"
    add_filter "/tests/"

    # does not work with Bashcov (?)
    minimum_coverage 100
end

SimpleCov.formatters = SimpleCov::Formatter::MultiFormatter.new([
  SimpleCov::Formatter::HTMLFormatter,
  SimpleCov::Formatter::CoberturaFormatter
])
